package phaser.example2;

import java.util.concurrent.Phaser;

public class Passenger implements Runnable
{
    private static final String WAIT  = " ждёт на остановке ";
    private static final String ENTER = " сел в автобус"   ;
    private static final String EXIT  = " вышел из автобуса ";
    private static final String SPACE = "\t\t";

    private final int id;
    private final BusStop departure;
    private final BusStop destination;
    private final Phaser phaser;

    public Passenger(int id, BusStop departure, BusStop destination, Phaser phaser)
    {
        this.id          = id;
        this.departure   = departure;
        this.destination = destination;
        this.phaser      = phaser;

        System.out.println(this + WAIT + departure);
    }

    public BusStop getDeparture() {
        return departure;
    }

    @Override
    public void run()  {
        try {
            System.out.println(SPACE + this + ENTER);

            if (phaser.getPhase() < destination.getBusStopId())
                phaser.arriveAndAwaitAdvance();

            Thread.sleep(500);

            System.out.println(SPACE + this + EXIT);

            phaser.arriveAndDeregister();
        } catch (InterruptedException exception) {
            System.out.println("Программа долго выполнялась и была прервана");
        }
    }

    @Override
    public String toString() {
        return String.format("Пассажир %d [%s -> %s]", id, departure, destination);
    }
}